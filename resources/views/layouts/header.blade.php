<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta name="copyright" content="Afgan Khalilov" />
  <meta name="author" content="Afgan Khalilov" />
  <meta name="keywords" content="Afgan Khalilov, hov-dev, hovdev, Afgan Khalilov Developer, HOV-DEV, akhalilov" />
  <meta property="og:type" content="article" />
  <meta property="og:description" content="Afgan Khalilov Personal Web" />
  <meta property="og:title" content="Personal @hovdev" />
  <meta property="og:url" content="https://hovdev.com" />
  <meta property="og:site_name" content="hovdev.com" />
  <meta property="og:image" content="https://hovdev.com/images/opgimage.png" />
  <meta property="og:image:secure_url" content="https://hovdev.com/images/opgimage.png" />
  <meta property="og:image:type" content="image/png" />
  <meta property="og:image:width" content="400" />
  <meta property="og:image:height" content="300" />
  <meta property="og:image:alt" content="hovdev.com official intro image" />





  <title>Personal @hovdev</title>
  <link rel="stylesheet" type="text/css" href="/css/app.css">

  <link rel="apple-touch-icon" sizes="57x57" href="/apple-icon-57x57.png">
  <link rel="apple-touch-icon" sizes="60x60" href="/apple-icon-60x60.png">
  <link rel="apple-touch-icon" sizes="72x72" href="/apple-icon-72x72.png">
  <link rel="apple-touch-icon" sizes="76x76" href="/apple-icon-76x76.png">
  <link rel="apple-touch-icon" sizes="114x114" href="/apple-icon-114x114.png">
  <link rel="apple-touch-icon" sizes="120x120" href="/apple-icon-120x120.png">
  <link rel="apple-touch-icon" sizes="144x144" href="/apple-icon-144x144.png">
  <link rel="apple-touch-icon" sizes="152x152" href="/apple-icon-152x152.png">
  <link rel="apple-touch-icon" sizes="180x180" href="/apple-icon-180x180.png">
  <link rel="icon" type="image/png" sizes="192x192"  href="/android-icon-192x192.png">
  <link rel="icon" type="image/png" sizes="32x32" href="/favicon-32x32.png">
  <link rel="icon" type="image/png" sizes="96x96" href="/favicon-96x96.png">
  <link rel="icon" type="image/png" sizes="16x16" href="/favicon-16x16.png">
  <link rel="manifest" href="/manifest.json">
  <meta name="msapplication-TileColor" content="#ffffff">
  <meta name="msapplication-TileImage" content="/ms-icon-144x144.png">
  <meta name="theme-color" content="#ffffff">


</head>
