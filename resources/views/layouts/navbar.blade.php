<nav class="navbar navbar-fixed-top navbar-inverse">
  <div class="container-fluid">
    <div class="navbar-header">
      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
      <a class="navbar-brand" href="/"><strong>hovdev.com</strong></a>
    </div>
    <div class="collapse navbar-collapse" id="myNavbar">
      <ul class="nav navbar-nav">
          <li class="active"><a href="#home">@lang("index.nav.home")</a></li>
          <li><a href="#about">@lang("index.nav.about")</a></li>
          <li><a href="#contact">@lang("index.nav.contacts")</a></li>
          <li><a href="#footer">@lang("index.nav.credits")</a></li>
          <li><a href="/ilikethat">@lang("index.nav.myfavorites")</a></li>
      </ul>
      <ul class="nav navbar-nav navbar-right">
        <li>
            <a href="/setlocale/az"><span class="lang-border">AZ</span></a>
        </li>
        <li>
            <a href="/setlocale/ru"><span class="lang-border">RU</span></a>
        </li>
        <li>
            <a href="/setlocale/en"><span class="lang-border">EN</span></a>
        </li>
      </ul>
    </div>
  </div>
</nav>
