<!-- About Me  -->
<section class="skills" id="skills">
    <div class="skills-cover">
        <div class="container">
            <div class="row">
                <div class="row-thumbnails">
                    <div class="col-sm-12">
                        <div class="hovdev-block">
                            <span class="hovdev-line">@lang('index.projects')</span>
                        </div>
                    </div>

                    <div class="col-sm-12">
                        <div class="row">
                            <div class="skill-parent">

                                <?php $projects = [
                                    'sahratravel' => [
                                        'name' => 'sahratravel.com',
                                        'code' => 'sahratravel',
                                        'link' => 'https://sahratravel.com'
                                    ],
                                    'insure' => [
                                        'name' => 'insure.az',
                                        'code' => 'insure',
                                        'link' => 'https://insure.az'
                                    ],
                                    'netstore' => [
                                        'name' => 'netstore.az',
                                        'code' => 'netstore',
                                        'link' => 'https://netstore.az'
                                    ],
                                    'layf' => [
                                        'name' => 'layf.az',
                                        'code' => 'layf',
                                        'link' => 'https://layf.az'
                                    ],
                                    'netchits' => [
                                        'name' => 'netchits.com',
                                        'code' => 'netchits',
                                        'link' => 'https://netchits.com'
                                    ],
                                    'onversus' => [
                                        'name' => 'onversus.fun',
                                        'code' => 'onversus',
                                        'link' => 'https://onversus.fun'
                                    ],
                                ]; ?>

                                <?php foreach ($projects as $key => $project) : ?>
                                    <div class="col-sm-4">
                                        <div class="project-item-block">
                                            <div class="<?php echo $project['code'] ?> project-img">
                                                <a href="{{ $project['link'] }}" target="__blank">
                                                    <div class="project-img-cover">
                                                        <div class="project-name">
                                                            {{ $project['name'] }}
                                                        </div>
                                                    </div>
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                <?php endforeach; ?>

                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
</section>
