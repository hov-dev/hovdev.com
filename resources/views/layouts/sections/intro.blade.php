<!-- Intro -->
<section class="intro intro-image" id="home">
    <div class="intro-cover">
        <div class="container">
        <div class="row row-intro">
            <div class="col-sm-12">
                <div class="row">
                    <div class="col-sm-12">
                        <h1 class="person-name"><strong>@lang("index.iam")<strong></h1>
                    </div>


                    <div class="col-sm-12">
                        <div class="row">
                            <div class="work-places-parent col-sm-6">
                                <h1 class="person-profession"><strong>@lang("index.profession")<strong></h1>
                                <div class="work-places">
                                    <span><a target="_blank" href="https://insure.az">insure.az</a></span>
                                    <span><a target="_blank" href="https://beylitech.az">beylitech.az</a></span>
                                </div>
                            </div>

                            <div class="work-places-parent col-sm-6">
                                <h1 class="person-profession"><strong>@lang("index.ceo")<strong></h1>
                                <div class="work-places">
                                    <span><a target="_blank" href="https://netchits.com">netchits.com</a></span>
                                    <span><a target="_blank" href="https://onversus.fun">onversus.fun</a></span>
                                </div>
                            </div>
                        </div>
                    </div>



                </div>

                <div class="row row-social">
                    <div class="col-sm-4 col-sm-offset-4">
                        <div class="row">
                            <div class="col-xs-4">
                                <a target="_blank" href="https://www.facebook.com/siler.aliyev" class="fa fa-facebook fa-facebook-head"></a>
                            </div>
                            <div class="col-xs-4">
                                <a target="_blank" href="https://twitter.com/thehovdev" class="fa fa-twitter fa-twitter-head twitter-col"></a>
                            </div>
                            <div class="col-xs-4">
                                <a target="_blank" href="https://t.me/thehovdev" class="fa fa-telegram fa-twitter-head twitter-col"></a>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    </div>
</section>
