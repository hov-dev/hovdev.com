<!-- About Me  -->
<section class="skills" id="skills">
    <div class="skills-cover">
        <div class="container">
            <div class="row">
                <div class="row-thumbnails">
                    <div class="col-sm-12">
                        <div class="hovdev-block">
                            <span class="hovdev-line">@lang('index.skills')</span>
                        </div>
                    </div>

                    <div class="col-sm-12">
                        <div class="row">
                            <div class="skill-parent">

                                <?php $skills = [
                                    'php',
                                    'wordpress',
                                    'laravel',
                                    'js',
                                    'node',
                                    'jquery',
                                    'vue',
                                    'bootstrap',
                                    'sass',
                                    'html',
                                    'css',
                                    'linux'
                                ]; ?>

                                <?php foreach ($skills as $key => $skill) : ?>
                                    <div class="col-sm-4">
                                        <div class="skill-item-block">
                                            <div class="<?php echo $skill ?> skill-img"></div>
                                        </div>
                                    </div>
                                <?php endforeach; ?>

                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
</section>
