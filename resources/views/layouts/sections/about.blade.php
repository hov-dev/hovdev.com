<!-- About Me  -->
<section class="about about-image" id="about">
    <div class="about-cover">

        <div class="container">
            <div class="row row-about">
                <div class="col-sm-12">
                    <div class="about-citate">
                        <p>@lang("index.about_citate")</p> <br>
                        <span class="author">Winston Churchill</span>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="row-thumbnails">
                    <div class="col-sm-6 col-xs-12">
                        <div class="author-block">
                        </div>
                    </div>
                    <div class="col-sm-6 col-xs-12">
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="author-info-block">
                                    <div class="info-item"><span class="heading">@lang('index.name')</span> Afgan</div>
                                    <div class="info-item"><span class="heading">@lang('index.lastname')</span> Khalilov</div>
                                    <div class="info-item"><span class="heading">@lang('index.phone')</span> +994513739930</div>
                                    <div class="info-item"><span class="heading">@lang('index.email')</span> hov-dev@protonmail.ch</div>
                                    <div class="info-item"><span class="heading">@lang('index.sitename')</span> https://hovdev.com</div>
                                </div>
                            </div>

                            <div class="col-sm-6">
                                <div class="author-info-block">
                                    <div class="info-item"><a target="_blank" href="https://facebook.com/thehovdev"><span class="heading">Facebook</span>thehovdev</a></div>
                                    <div class="info-item"><a target="_blank" href="https://twitter.com/thehovdev"><span class="heading">Twitter</span>thehovdev</a></div>
                                    <div class="info-item"><a target="_blank" href="https://t.me/thehovdev"><span class="heading">Telegram</span>thehovdev</a></div>
                                    <div class="info-item"><a target="_blank" href="https://github.com/thehovdev"><span class="heading">Github</span>thehovdev</a></div>
                                    <div class="info-item"><a target="_blank" href="https://www.netchits.com/user/38"><span class="heading">Netchits</span>#hovdev</a></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            {{-- <div class="row row-arsenal">

                <div class="col-xs-6 col-sm-6">


                    <div class="row">
                        <div class="col-sm-6">
                            <img src="{{ asset('/images/userimg.jpg') }}" class="img-circle img-responsive"/>
                        </div>
                        <div class="col-sm-6">
                            <ul class="list-group personal-group">
                                <li class="list-group-item"><u>@lang("index.fullname")</u></li>
                                <li class="list-group-item">@lang("index.author")</li>
                                <li class="list-group-item"><u>@lang("index.birthdate")</u></li>
                                <li class="list-group-item number">12/22/1996</li>
                                <li class="list-group-item"><u>@lang("index.job")</u></li>
                                <li class="list-group-item">@lang("index.profession")</li>
                                <li class="list-group-item"><u>@lang("index.email")</u></li>
                                <li class="list-group-item">hov-dev@protonmail.ch</li>
                                <li class="list-group-item"><u>@lang("index.sitename")</u></li>
                                <li class="list-group-item">www.hov-dev.com</li>
                                <li class="list-group-item"><u>@lang("index.phone")</u></li>
                                <li class="list-group-item phone">+994513739930</li>
                            </ul>
                        </div>
                    </div>


                </div>


                <div class="col-xs-4 col-xs-offset-1 col-sm-4 col-sm-offset-1">
                      <div class="row">

                          <div class="row row-arsenal-header">
                              <div class="col-sm-6 col-sm-offset-2">
                                  <h3>@lang("index.arsenal")</h3>
                              </div>
                          </div>

                          <div class="row">

                              <div class="col-sm-6">
                                  <ul class="list-group arsenal-group">
                                      <li class="list-group-item">PHP</li>
                                      <li class="list-group-item">Laravel</li>
                                      <li class="list-group-item">Javascript</li>
                                      <li class="list-group-item">Jquery</li>
                                      <li class="list-group-item">Wordpress</li>
                                      <li class="list-group-item">1c-Bitrix</li>
                                  </ul>
                              </div>
                              <div class="col-sm-6">
                                  <ul class="list-group arsenal-group">
                                      <li class="list-group-item">Bootstrap</li>
                                      <li class="list-group-item">HTML</li>
                                      <li class="list-group-item">CSS</li>
                                      <li class="list-group-item">GIT</li>
                                      <li class="list-group-item">Linux</li>
                                  </ul>
                              </div>
                          </div>
                      </div>
                </div>
            </div> --}}


    </div>
</section>
