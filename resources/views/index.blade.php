<!DOCTYPE html>
<html lang="en">

    @include("layouts.header")

    <body>

            @include('layouts.navbar')

            @include('layouts.sections.intro')

            @include('layouts.sections.about')

            {{-- @include('layouts.sections.projects') --}}

            @include('layouts.sections.skills')

            <!-- footer -->
            <section class="footer footer-image" id="footer">
                <div class="container">
                    <div class="row row-footer">
                        <div class="col-xs-12 col-sm-12">
                            <div class="row">
                                <div class="col-xs-12 col-sm-4">
                                    <row>
                                        <div class="col-sm-12 mail-column">
                                            <i class="fa fa-envelope-o"></i>
                                        </div>
                                        <div class="col-sm-12 mail-text">
                                            <h3>@lang("index.emailme")</h3>
                                        </div>
                                        <div class="col-sm-12 mail-text">
                                            <h5>hov-dev@protonmail.ch</h5>
                                        </div>
                                    </row>
                                </div>
                                <div class="col-xs-12 col-sm-4">
                                    <row>
                                        <div class="col-sm-12 mail-column">
                                            <i class="fa fa-phone"></i>
                                        </div>
                                        <div class="col-sm-12 mail-text">
                                            <h3>@lang("index.callme")</h3>
                                        </div>
                                        <div class="col-sm-12 mail-text">
                                            <h5 class="phone">+994513739930</h5>
                                        </div>
                                    </row>
                                </div>
                                <div class="col-xs-12 col-sm-4">
                                    <row>
                                        <div class="col-sm-12 mail-column">
                                            <i class="fa fa-map-marker"></i>
                                        </div>
                                        <div class="col-sm-12 mail-text">
                                            <h3>@lang("index.findme")</h3>
                                        </div>
                                        <div class="col-sm-12 mail-text">
                                            <h5>J.M.Guluzade 30, Baku, Azerbaijan</h5>
                                        </div>
                                    </row>
                                </div>
                            </div>
                        </div>

                        <div class="col-xs-12 col-sm-12 authors">
                            <div class="row">
                                <div class="col-xs-6 col-sm-6">
                                    <div class="row">
                                        <div class="col-xs-12 col-xs-offset-0 col-sm-4 col-sm-offset-3">
                                            <h5 style="text-align:left !important;">&copy;Copyright @php echo date('Y'); @endphp</h5>
                                        </div>
                                        <div class="col-xs-12 col-sm-5">
                                            <h5 style="text-align:left !important;">@lang("index.developed")</h5>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-xs-6 col-sm-6">
                                    <div class="row row-social-footer">
                                        <div class="col-xs-4 col-sm-4 col-sm-offset-4">
                                            <div class="row">
                                                <div class="col-sm-4">
                                                    <a target="__blank" href="https://www.facebook.com/thehovdev" class="fa fa-facebook fa-facebook-footer"></a>
                                                </div>
                                                <div class="col-sm-4">
                                                    <a target="__blank" href="https://twitter.com/thehovdev" class="fa fa-twitter fa-twitter-footer"></a>
                                                </div>
                                                <div class="col-sm-4">
                                                    <a target="__blank" href="https://t.me/thehovdev" class="fa fa-telegram fa-telegram-footer"></a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </section>


    </body>

    @include("layouts.footer")

</html>
