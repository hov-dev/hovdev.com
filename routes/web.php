<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('setlocale/{locale}', function ($locale) {
    # Проверяем, что у пользователя выбран доступный язык
    # И устанавливаем его в сессии под именем locale
    if (in_array($locale, \Config::get('app.locales'))) {
    	Session::put('locale', $locale);
    }
    // dd(Session::get('locale'));
    return redirect()->back();
});

Route::any('/ilikethat', function() {
    $favorites = [
        'https://insure.az/en',
        'https://stackoverflow.com/',
        'https://www.w3schools.com',
        'https://habrahabr.ru/',
        'https://toster.ru/',
        'https://laravel.com/',
        'http://php.net/',
        'https://protonmail.com/',
        'https://gitlab.com/',
        'https://github.com',
        'https://www.networksolutions.com/',
        'https://www.digitalocean.com/',
    ];

    $random = $favorites[array_rand($favorites, 1)];

    return Redirect::to($random);
});

Route::get('/', function () {
    return view('index');
});
Route::any('/download-cv', function() {
    $pathToFile = "images/user.jpg";
    return response()->download($pathToFile);
});
Route::any('/getFeedback', 'FeedbackController@index');
